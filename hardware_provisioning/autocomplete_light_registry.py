# -*- coding: utf-8 -*-

import autocomplete_light
from models import Item


# This will generate a MemberAutocomplete class
autocomplete_light.register(Item,
                            # Just like in ModelAdmin.search_fields
                            search_fields=[
                                'designation', 'mac_address', 'serial'],
                            attrs={
                                # This will set the input placeholder attribute:
                                'placeholder': "Nom / adresse MAC / n° de série de l'objet",
                                'data-autocomplete-minimum-characters': 3,
                            },
)
