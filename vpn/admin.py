# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.contrib import admin
from polymorphic.admin import PolymorphicChildModelAdmin

from coin.configuration.admin import ConfigurationAdminFormMixin
from coin.utils import delete_selected

from .models import VPNConfiguration


class VPNConfigurationInline(admin.StackedInline):
    model = VPNConfiguration
    # fk_name = 'offersubscription'
    exclude = ('password',)
    readonly_fields = ['configuration_ptr', 'login']


class VPNConfigurationAdmin(ConfigurationAdminFormMixin, PolymorphicChildModelAdmin):
    base_model = VPNConfiguration
    list_display = ('offersubscription', 'activated', 'login',
                    'ipv4_endpoint', 'ipv6_endpoint', 'comment')
    list_filter = ('activated',)
    search_fields = ('login', 'comment',
                     # TODO: searching on member directly doesn't work
                     'offersubscription__member__first_name',
                     'offersubscription__member__last_name',
                     'offersubscription__member__email')
    actions = (delete_selected, "generate_endpoints", "generate_endpoints_v4",
               "generate_endpoints_v6", "activate", "deactivate")
    exclude = ("password",)
    inline = VPNConfigurationInline

    def get_readonly_fields(self, request, obj=None):
        if obj:
            return ['login',]
        else:
            return []

    def set_activation(self, request, queryset, value):
        count = 0
        # We must update each object individually, because we want to run
        # the save() method to update the backend.
        for vpn in queryset:
            if vpn.activated != value:
                vpn.activated = value
                vpn.full_clean()
                vpn.save()
                count += 1
        action = "activated" if value else "deactivated"
        msg = "{} VPN subscription(s) {}.".format(count, action)
        self.message_user(request, msg)

    def activate(self, request, queryset):
        self.set_activation(request, queryset, True)
    activate.short_description = "Activer les VPN sélectionnés"

    def deactivate(self, request, queryset):
        self.set_activation(request, queryset, False)
    deactivate.short_description = "Désactiver les VPN sélectionnés"

    def generate_endpoints_generic(self, request, queryset, v4=True, v6=True):
        count = 0
        for vpn in queryset:
            if vpn.generate_endpoints(v4, v6):
                vpn.full_clean()
                vpn.save()
                count += 1
        msg = "{} VPN subscription(s) updated.".format(count)
        self.message_user(request, msg)

    def generate_endpoints(self, request, queryset):
        self.generate_endpoints_generic(request, queryset)
    generate_endpoints.short_description = "Attribuer des adresses IPv4 et IPv6"

    def generate_endpoints_v4(self, request, queryset):
        self.generate_endpoints_generic(request, queryset, v6=False)
    generate_endpoints_v4.short_description = "Attribuer des adresses IPv4"

    def generate_endpoints_v6(self, request, queryset):
        self.generate_endpoints_generic(request, queryset, v4=False)
    generate_endpoints_v6.short_description = "Attribuer des adresses IPv6"

admin.site.register(VPNConfiguration, VPNConfigurationAdmin)
