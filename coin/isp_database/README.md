# ISP database application

The goal of this application is to generate a JSON file describing an ISP,
as defined by http://db.ffdn.org/format

Most information about the ISP can be entered through the admin.  The number
of members and subscribers is computed automatically: you may want to change
the way they are computed depending on the way you manage members and
subscribers.  Additionally, it is possible to change the model so that you
manage the number of members and subscribers by hand in the admin, though
this is not recommended.

The JSON file is then accessible at /isp.json

## How to use

First, add `coin.isp_database` to your `INSTALLED_APPS` in settings.py.

Then, add the following in your main `urls.py`:

    from coin.isp_database.views import isp_json

and add

    url(r'^isp.json$', isp_json),

in your `urlpatterns` variable.

## Known issues

Grep for "TODO" in the code.

- the "progress status" field is not user-friendly
- the model for GPS coordinates is not good (no validation)
- currently, only one technology is allowed for each covered area
- describing the geographical shape (polygone) of a covered area is not supported
