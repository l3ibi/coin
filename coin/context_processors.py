from django.conf import settings


def installed_apps(request):
    """ Expose the settings INSTALLED_APPS to templates
    """
    return {'INSTALLED_APPS': settings.INSTALLED_APPS}
