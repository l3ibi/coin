# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.conf.urls import patterns, url

from .views import DSLView


urlpatterns = patterns(
    '',
    # This is part of the generic configuration interface (the "name" is
    # the same as the "backend_name" of the model).
    url(r'^(?P<pk>\d+)$', DSLView.as_view(template_name="dsl_ldap/dsl.html"),
        name="details")
)
