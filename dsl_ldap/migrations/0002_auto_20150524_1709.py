# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('dsl_ldap', '0001_initial'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='dslconfiguration',
            options={'verbose_name': 'DSL line', 'verbose_name_plural': 'DSL lines'},
        ),
        migrations.AlterField(
            model_name='dslconfiguration',
            name='login',
            field=models.CharField(help_text='Leave empty for automatic generation', unique=True, max_length=50, verbose_name='login', blank=True),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='dslconfiguration',
            name='password',
            field=models.CharField(help_text='Will be stored in cleartext!  Automatically generated if empty', max_length=256, verbose_name='password', blank=True),
            preserve_default=True,
        ),
    ]
