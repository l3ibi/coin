# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.apps import AppConfig
import coin.apps

from . import urls


class DSLLDAPConfig(AppConfig, coin.apps.AppURLs):
    name = 'dsl_ldap'
    verbose_name = "xDSL (LDAP)"

    exported_urlpatterns = [('dsl_ldap', urls.urlpatterns)]
